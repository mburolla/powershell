# Powershell
- `Set-ExecutionPolicy RemoteSigned -Scope CurrentUser`
- To execute: `.\clone-java-repos.ps1`

# Command Reference
[CmdLets](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.management/new-item?view=powershell-7.1)

[Cheat Sheet](https://www.comparitech.com/net-admin/powershell-cheat-sheet/)