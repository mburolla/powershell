$RootDir = "XPX"
Write-Host "************************************************"
Write-Host "***        Cloning Java Repositories         ***"
Write-Host "************************************************"
Write-Warning "This clones Git repositories to C:\$RootDir." -WarningAction Inquire

# Create directories
New-Item -Path "C:\$RootDir\Java\Java-I" -ItemType "directory"
New-Item -Path "C:\$RootDir\Java\Java-II" -ItemType "directory"
New-Item -Path "C:\$RootDir\Java\Java-III" -ItemType "directory"
New-Item -Path "C:\$RootDir\Java\Java-Data Access" -ItemType "directory"
New-Item -Path "C:\$RootDir\Java\Java-Building Packaging" -ItemType "directory"
New-Item -Path "C:\$RootDir\Java\Java-Frameworks Concepts" -ItemType "directory"

# Clone repositories
Set-Location -Path "C:\$RootDir\Java\Java-I"
Invoke-Expression -Command "git clone git@gitlab.com:mburolla/java-intro.git"

Set-Location -Path "C:\$RootDir\Java\Java-II"
Invoke-Expression -Command "git clone git@gitlab.com:mburolla/java-di.git"
Invoke-Expression -Command "git clone git@gitlab.com:mburolla/java-oop.git"
Invoke-Expression -Command "git clone git@gitlab.com:mburolla/java-collections.git"

Set-Location -Path "C:\$RootDir\Java\Java-III"
Invoke-Expression -Command "git clone git@gitlab.com:mburolla/java-file-io.git"
Invoke-Expression -Command "git clone git@gitlab.com:mburolla/java-ex-ut-logs.git"

Set-Location -Path "C:\$RootDir\Java\Java-Data Access"
Invoke-Expression -Command "git clone git@gitlab.com:mburolla/java-no-sql.git"
Invoke-Expression -Command "git clone git@gitlab.com:mburolla/java-adhoc-sql.git"
Invoke-Expression -Command "git clone git@gitlab.com:mburolla/java-hibernate.git"

Set-Location -Path "C:\$RootDir\Java\Java-Frameworks Concepts"
Invoke-Expression -Command "git clone git@gitlab.com:mburolla/java-spring-boot-web-api.git"

# Set-Location -Path "C:\$RootDir\Java\Java-Building Packaging"
# Invoke-Expression -Command "git clone "

# Reset Location
Set-Location  -Path "C:\vc\gitlab\powershell"
